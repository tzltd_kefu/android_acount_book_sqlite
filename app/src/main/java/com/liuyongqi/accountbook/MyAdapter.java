package com.liuyongqi.accountbook;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.liuyongqi.accountbook.R;

public class MyAdapter extends BaseAdapter {

    private Context context;
    private Cursor cursor;
    private LinearLayout layout;

    public MyAdapter(Context context, Cursor cursor) {
        this.context = context;
        this.cursor = cursor;
    }
    @Override
    public int getCount() {
        return cursor.getCount();
    }

    @Override
    public Object getItem(int position) {
        return cursor.getPosition();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        layout = (LinearLayout) inflater.inflate(R.layout.item, null);
        TextView tv_id = (TextView) layout.findViewById(R.id.tv_id);
        TextView tv_type = (TextView) layout.findViewById(R.id.tv_type);
        TextView tv_time = (TextView) layout.findViewById(R.id.tv_path);
        TextView tv_money = (TextView) layout.findViewById(R.id.tv_money);
        LinearLayout ll = (LinearLayout) layout.findViewById(R.id.ll);

        cursor.moveToPosition(position);
        String content = cursor.getString(cursor.getColumnIndex("content"));
        String money = cursor.getString(cursor.getColumnIndex("money"));
        String path = cursor.getString(cursor.getColumnIndex("path"));
        String id = cursor.getString(cursor.getColumnIndex("_id"));
        tv_type.setText(content);
        tv_time.setText(path);
        tv_money.setText(money);
        tv_id.setText(id);
        return layout;
    }
}