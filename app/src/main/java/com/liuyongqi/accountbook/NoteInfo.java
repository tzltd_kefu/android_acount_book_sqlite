package com.liuyongqi.accountbook;


public class NoteInfo {
    private String type;
    private String money;
    private String path;

    public NoteInfo(String type, String money, String path) {
        this.type = type;
        this.money = money;
        this.path = path;
    }

    public String getType() {

        return type;
    }

    public void setType(String type) {

        this.type = type;
    }

    public String getMoney() {

        return money;
    }

    public void setMoney(String money) {

        this.money = money;
    }

    public String getPath() {

        return path;
    }

    public void setPath(String path) {

        this.path = path;
    }


}
