package com.liuyongqi.accountbook;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Select extends AppCompatActivity {
    private Button select,start;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);
        start = findViewById(R.id.start);
        select = findViewById(R.id.select);
        //支出
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Select.this,AllActivity.class);
                intent.putExtra("type","1");
                startActivity(intent);
            }
        });
        //收入
        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Select.this,AllActivity.class);
                intent.putExtra("type","0");
                startActivity(intent);
            }
        });
    }
}