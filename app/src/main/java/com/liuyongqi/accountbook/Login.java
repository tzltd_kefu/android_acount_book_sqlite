package com.liuyongqi.accountbook;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class Login extends AppCompatActivity {

    private EditText et_name;
    private EditText et_pw;
    private CheckBox cb_rm;
    private SharedPreferences sp;
    Button loginbtn , registbtn ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initView();

        sp = getSharedPreferences("menu", Context.MODE_PRIVATE);
        showInfo();
    }
    private void showInfo() {
        String username = sp.getString("username","");//map只需要一个key sp还有一个参数
        String pw = sp.getString("pw","");
        Boolean isChecked = sp.getBoolean("isChecked",false);
        et_name.setText(username);
        et_pw.setText(pw);
        cb_rm.setChecked(isChecked);

    }
    private void initView() {
        et_name = (EditText) findViewById(R.id.et_name);
        et_pw = (EditText) findViewById(R.id.et_pw);
        cb_rm= (CheckBox) findViewById(R.id.cb_rm);
        loginbtn = findViewById(R.id.login);
        registbtn = findViewById(R.id.register);
        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
        registbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                regist();
            }
        });
    }
    void regist () {
        startActivity(new Intent(Login.this,AddUser.class));
    }
    void login (){
        String rm = cb_rm.getText().toString().trim();
        String name = et_name.getText().toString().trim();
        String pw = et_pw.getText().toString().trim();//控件，取文本
        if(name.isEmpty()|| TextUtils.isEmpty(pw)){
            MyToast.show(this,"输入的内容不能为空");
            return;
        }
        boolean isChecked = cb_rm.isChecked();
        if (!isChecked){
            MyToast.show(this,"请记住密码");
            return;
        }
        boolean isSuccess = saveUserInfoWithSP(name, pw,isChecked);
        UserDBHelper userDBHelper = new UserDBHelper(this);
        SQLiteDatabase database = userDBHelper.getReadableDatabase();
        Cursor cursor = database.query ("user",null,null,null,null,null,null);
        while(cursor.moveToNext()){
            String itea_name = cursor.getString(cursor.getColumnIndex("name"));
            String itea_pass = cursor.getString(cursor.getColumnIndex("pass"));
            Log.d("TAG", "login: " + "query------->" + "姓名："+itea_name+" "+"itea_pass："+itea_pass+" ");
            if(itea_name.equals(name) && itea_pass.equals(pw)){
                MyToast.show(this,"登陆成功");
                Intent intent = new Intent(Login.this,MainActivity.class);
                startActivity(intent);
                finish();
                return;
            }
        }


        MyToast.show(this,"登陆失败");


    }

    private boolean saveUserInfoWithSP(String username,String pw,Boolean isChecked) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean("isChecked",isChecked);//editor是对map进行封装 限制用户输入的类型
        editor.putString("username",username);
        editor.putString("pw",pw);
        boolean isSuccess = editor.commit();//提交commit
        return isSuccess;
    }


}
