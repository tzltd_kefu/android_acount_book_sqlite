package com.liuyongqi.accountbook;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;


public class DetailAdapter extends BaseAdapter {
    private Context context;
    private List<NoteInfo> list;
    private ViewHolder viewHolder;

    public DetailAdapter(Context context,List<NoteInfo> list){

        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null){
            convertView = View.inflate(this.context,R.layout.item,null);

            viewHolder = new ViewHolder();

            viewHolder.tv_money = (TextView)convertView.findViewById(R.id.tv_money);
            viewHolder.tv_path = (TextView) convertView.findViewById(R.id.tv_path);
            viewHolder.tv_name = (TextView) convertView.findViewById(R.id.tv_type);
            viewHolder.ll = (LinearLayout) convertView.findViewById(R.id.ll);
            convertView.setTag(viewHolder);

        }else {
            viewHolder = (ViewHolder) convertView.getTag();

        }
        viewHolder.tv_money.setText(list.get(position).getMoney());
        viewHolder.tv_path.setText(list.get(position).getPath());
        viewHolder.tv_name.setText(list.get(position).getType());
        viewHolder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Detail.class);
                intent.putExtra(NotesDBHelper.ID, position);
                intent.putExtra(NotesDBHelper.TYPE, list.get(position).getType());
                intent.putExtra(NotesDBHelper.MONEY,  list.get(position).getMoney());
                intent.putExtra(NotesDBHelper.PATH,  list.get(position).getPath());
                context.startActivity(intent);
            }
        });
        return convertView;
    }

    public static class  ViewHolder{
        TextView tv_name,tv_path,tv_money;
        LinearLayout ll;
    }
}
