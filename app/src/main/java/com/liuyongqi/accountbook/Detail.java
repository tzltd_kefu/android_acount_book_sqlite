package com.liuyongqi.accountbook;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Detail extends AppCompatActivity {

    private NotesDBHelper helper;
    private SQLiteDatabase db;
    private Button select,start;
    private EditText et_money,et_path,et_type;
    private TextView tv_type;
    String id = "";
    private EditText et_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        start = findViewById(R.id.start);
        et_id = (EditText) findViewById(R.id.et_id);
        select = findViewById(R.id.select);
        et_money = findViewById(R.id.et_money);
        et_type = findViewById(R.id.et_type);
        et_path = findViewById(R.id.et_path);
        tv_type = findViewById(R.id.tv_type);


        helper = new NotesDBHelper(this);
        db = helper.getWritableDatabase();
        id = getIntent().getStringExtra(NotesDBHelper.ID);
        et_type.setText(getIntent().getStringExtra(NotesDBHelper.TYPE));
        et_money.setText(getIntent().getStringExtra(NotesDBHelper.MONEY));
        et_path.setText(getIntent().getStringExtra(NotesDBHelper.PATH));
        et_id.setText(getIntent().getStringExtra(NotesDBHelper.ID));

        //删除
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDate();
                Toast.makeText(Detail.this,"删除成功",Toast.LENGTH_SHORT).show();
                finish();
            }
        });
        //修改
        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentValues cv = new ContentValues();
                cv.put(NotesDBHelper.ID, et_id.getText().toString());
                cv.put(NotesDBHelper.TYPE, et_type.getText().toString());
                cv.put(NotesDBHelper.MONEY, et_money.getText().toString());
                cv.put(NotesDBHelper.PATH, et_path.getText().toString());
                db.update(NotesDBHelper.TABLE_NAME,cv,"_id=?",new String[]{et_id.getText().toString()});
                  Toast.makeText(Detail.this,"修改成功",Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    public void deleteDate() {
        db.delete(NotesDBHelper.TABLE_NAME,
                "_id=" + getIntent().getIntExtra(NotesDBHelper.ID, 0), null);
    }
}