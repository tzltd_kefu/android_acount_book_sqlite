package com.liuyongqi.accountbook;


import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class UserOP {
    @Deprecated //不建议使用
    public static boolean saveUserInfo(String userName,String pw){//static 工具
        String np=userName+"--"+pw;
        File file = new File("/data/data/com.sxt.flowerroad/login.txt");
        FileOutputStream fos = null;//初始化
        try {
            fos = new FileOutputStream(file);//ctrl alt v
            fos.write(np.getBytes());
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                fos.close();
            } catch (IOException e) {//因为io关不上所以报错 用catch
                e.printStackTrace();
            }catch(Exception e){//空指针异常
                e.printStackTrace();
            }

        }
        return false;
    }

    @Deprecated
    public static Map<String,String> readUserInfo(){
        //map一对一映射（key（键）和values（值）对应），静态方法只能访问静态变量
        Map<String,String> user=new HashMap<>();//map是接口不能new HashMap是他的子类
        String path="/data/data/com.sxt.flowerroad/login.txt";
        String content = getContentFromFile(path);
        String[] split = content.split("--");//split（分开）根据某个标记分开
        if(split.length==2){
            user.put("userName",split[0]);
            user.put("pw",split[1]);
        }
        return user;
    }

    private static String getContentFromFile(String path) {
        String content= "";//空字符串，不会返回空指针
        File file = new File(path);
        FileInputStream fis=null;
        try {
            fis = new FileInputStream(file);//写的慢 读太快了 怕卡所以用buffer缓冲一下
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            content = reader.readLine();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }catch(Exception e){
                e.printStackTrace();
            }
        }


        return content;
    }
    private static String getContentFromFile(String parent,String child){
        String content="";
        File file = new File(parent,child);
        FileInputStream fis=null;
        try {//BufferReader 缓存
            fis = new FileInputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            content=reader.readLine();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return content;
    }

    //////////////////////////////////////
    public static boolean saveUserInfo(Context context, UserInfo userInfo){
        String np=userInfo.getUser_name()+"##"+userInfo.getPassword();
        String path=context.getFilesDir().getPath(); //相对路径
        File file = new File(path,"userInfo.txt");
        FileOutputStream fos=null;
        try {
            fos = new FileOutputStream(file);
            fos.write(np.getBytes());
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        return false;
    }
    public static UserInfo readUserInfo(Context context){
        UserInfo userInfo = new UserInfo();
        String path=context.getFilesDir().getPath();
        String content = getContentFromFile(path,"userInfo.txt");
        String[] split = content.split("##");
        if(split.length==2){
            userInfo.setUser_name(split[0]);
            userInfo.setPassword(split[1]);
        }
        return userInfo;
    }

}

