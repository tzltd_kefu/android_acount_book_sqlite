package com.liuyongqi.accountbook;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AddUser extends AppCompatActivity {
    private Button regist;
    private EditText et_name,et_pw,et_pw2;

    private SQLiteDatabase dbWriter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regist);

        regist = findViewById(R.id.register);
        et_name = findViewById(R.id.et_name);
        et_pw = findViewById(R.id.et_pw);
        et_pw2  = findViewById(R.id.et_pw2);
        regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDB();
            }
        });

    }

    public void addDB() {
        String name = et_name.getText().toString().trim();
        String pw = et_pw.getText().toString().trim();//控件，取文本
        String pw2 = et_pw2.getText().toString().trim();//控件，取文本
        if(!pw.equals(pw2)){
            MyToast.show(this,"两次密码不正确！");
            return;
        }
        if(name.isEmpty()|| TextUtils.isEmpty(pw)){
            MyToast.show(this,"输入的内容不能为空");
            return;
        }

        UserDBHelper userDBHelper = new UserDBHelper(this);
        SQLiteDatabase database = userDBHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
//往ContentValues对象存放数据，键-值对模式
        cv.put("name", name);
        cv.put("pass", pw);
//调用insert方法，将数据插入数据库
        long res= database.insert("user", null, cv);
//关闭数据库
        database.close();
        if (res > 0){
            MyToast.show(this,"注册成功");

            finish();
        }else{
            MyToast.show(this,"注册失败");
        }
    }
}
