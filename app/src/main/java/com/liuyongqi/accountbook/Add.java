package com.liuyongqi.accountbook;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Add extends AppCompatActivity {
    private Button select;
    private Button start;
    private EditText et_money,et_path,et_type;
    private TextView tv_type;

    private NotesDBHelper helper;
    private SQLiteDatabase dbWriter;
    String type = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        start = findViewById(R.id.start);
        select = findViewById(R.id.select);
        et_money = findViewById(R.id.et_money);
        et_type = findViewById(R.id.et_type);
        et_path = findViewById(R.id.et_path);
        tv_type = findViewById(R.id.tv_type);
        helper = new NotesDBHelper(this);
        dbWriter = helper.getWritableDatabase();
         type = getIntent().getStringExtra("type");
        if ("0".equals(type)){
            tv_type.setText("添加收入");
            et_type.setText("收入");
        }else {
            tv_type.setText("添加支出");
            et_type.setText("支出");
        }
        //取消
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        //添加
        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDB();
                Toast.makeText(Add.this,"添加成功",Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    public void addDB() {
        ContentValues cv = new ContentValues();
        cv.put(NotesDBHelper.TYPE, et_type.getText().toString());
        cv.put(NotesDBHelper.MONEY, et_money.getText().toString());
        cv.put(NotesDBHelper.PATH, et_path.getText().toString());
        dbWriter.insert(NotesDBHelper.TABLE_NAME, null, cv);
    }
}