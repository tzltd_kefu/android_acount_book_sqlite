package com.liuyongqi.accountbook;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class Splash extends AppCompatActivity {
    private int time = 2;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        handler.postDelayed(runnable, 2000);
    }


    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            time--;
            handler.postDelayed(this, 1500);
            if (time == 0) {
                goToNextActivity();
            } else {

            }
        }

        private void goToNextActivity() {
            //结束线程
            handler.removeCallbacks(runnable);
            startActivity(new Intent(Splash.this, Login.class));
            finish();
        }
    };

}