package com.liuyongqi.accountbook;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class NotesDBHelper extends SQLiteOpenHelper {
    public static final String TABLE_NAME = "notes";
    public static final String TYPE = "content";
    public static final String MONEY = "money";
    public static final String PATH = "path";
    public static final String ID = "_id";

    public NotesDBHelper(Context context) {
        super(context, "notes", null, 3);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + "(" + ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT," + TYPE
                + " TEXT NOT NULL," + MONEY + " TEXT NOT NULL," + PATH
                + " TEXT NOT NULL)");
        //db.insert优点 有返回值可以用户交互 不用写sql语句 单表操作
        //db.execSQL优点 能写多个表
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}
