package com.liuyongqi.accountbook;

import android.content.Context;
import android.widget.Toast;

public class MyToast {
    public static void show(Context context, String content){
        Toast.makeText(context, content, Toast.LENGTH_SHORT).show();
    }
}
