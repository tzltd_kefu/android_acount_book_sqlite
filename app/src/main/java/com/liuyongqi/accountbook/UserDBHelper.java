package com.liuyongqi.accountbook;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;



public class UserDBHelper extends SQLiteOpenHelper {
    public static final String PASSWORD = "pass";
    public static final String LOGIN = "user";
    public static final String USER_NAME ="name" ;


    public UserDBHelper( Context context) {
        super(context, "login", null, 3);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + LOGIN + "("
                + "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + USER_NAME +" TEXT NOT NULL ,"
                + PASSWORD +" TEXT NOT NULL)"
        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
