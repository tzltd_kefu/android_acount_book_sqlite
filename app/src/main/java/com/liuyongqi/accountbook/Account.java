package com.liuyongqi.accountbook;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Account extends AppCompatActivity {
    private Button select;
    private Button start;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        start = findViewById(R.id.start);
        select = findViewById(R.id.select);
        //收入
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Account.this, Add.class);
                intent.putExtra("type","0");
                startActivity(intent);
            }
        });
        //支出
        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Account.this, Add.class);
                intent.putExtra("type","1");
                startActivity(intent);
            }
        });
    }


}